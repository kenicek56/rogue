﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menu : MonoBehaviour {
	private GameObject StartMenu;
	private bool settingUpGame;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public Canvas quitMenu;
	public Button startText;
	public Button exitText;

	// Use this for initialization
	void Start () 
	{
		quitMenu = quitMenu.GetComponent<Canvas> ();
		startText = startText.GetComponent<Button> ();
		exitText = exitText.GetComponent<Button> ();
		quitMenu.enabled = false;
	
	}

	public void ExitPress()
	{
		quitMenu.enabled = true;
		startText.enabled = false;
		exitText.enabled = false;
	
	}

	public void NoPress()
	{
		quitMenu.enabled = false;
		startText.enabled = true;
		exitText.enabled = true;
	}

	public void StartLevel()
	{
		//GameController.Instance.InitializeGame();
		DisableLevelImage ();
	}
	private void DisableLevelImage()
	{
		StartMenu = GameObject.Find ("StartMenu");
		StartMenu.SetActive(false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}

	public void ExitGame()
	{
		Application.Quit();
	}


}
